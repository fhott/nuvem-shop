import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom'

import Sidebar from 'Components/Sidebar'
import TopHeaderMobile from 'Components/TopHeaderMobile'
import ProductsNew from 'Modules/ProductsNew'
import ProductsList from 'Modules/ProductsList'

import './App.sass'

export default class App extends React.Component {
  constructor() {
    super()

    this.state = {
      openSidebar: false
    }
  }

  openSidebarHandler() {
    const sidebarStatus = this.state.openSidebar

    this.setState({ openSidebar: !sidebarStatus })
  }

  render() {
    return (
      <Router>
        <main className="app">
          <TopHeaderMobile
            openSidebar={this.openSidebarHandler.bind(this)}
            open={this.state.openSidebar}
          />
          <Sidebar
            openSidebar={this.openSidebarHandler.bind(this)}
            open={this.state.openSidebar}
          />
          <Switch>
            <Route
              exact
              path="/"
              render={() => <Redirect to="/products"/>}
            />
            <Route
              exact
              path="/products"
              component={ProductsList}
            />
            <Route exact path="/product/new" component={ProductsNew} />
          </Switch>
        </main>
      </Router>
    );
  }
}
