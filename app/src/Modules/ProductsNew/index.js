import React from 'react'
import FieldText from '@atlaskit/field-text'
import Button from '@atlaskit/button'
import { Redirect } from 'react-router-dom'
import Form, { Field, Validator } from '@atlaskit/form'
import Content from 'Components/Content'

import addPhoto from 'Assets/Images/add-camera.png'
import './ProductNew.sass'

export default class ProductsNew extends React.Component {
  constructor() {
    super()

    this.state = {
      name: "",
      price: 0,
      file: '',
      stock: 0,
      submitted: false,
      redirect: false,
      imageURL: ''
    }

    this.breadcrumbs = [
      {
        text: "Meus produtos",
        link: "/products"
      },  {
        text: "Adicionar um novo produto",
        link: "/product/new"
      }
    ]
  }

  uploadFile(e) {
    e.preventDefault();

    const data = new FormData();
    data.append('file', this.uploadInput.files[0])

    fetch('http://localhost:4000/upload', {
      method: 'POST',
      body: data,
    }).then(response => response.json())
      .then(response => this.setState({ imageURL: `http://localhost:4000/files/${response.file}` }))
  }

  onSubmitHandler() {
    this.setState({ submittet: true })

    fetch('http://localhost:4000/product',
      { method: 'POST',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        cache: 'default',
        body: JSON.stringify({ product: {
          name: this.state.name,
          price: this.state.price,
          stock: this.state.stock,
          imageUrl: this.state.imageURL,
        }})
      }).then(() => this.setState({ redirect: true }))
  }

  render() {
    return [
      this.state.redirect && <Redirect key="redirect" to="/products"/>,
      <Content
        key="content"
        breadcrumbs={this.breadcrumbs}
      >
        <h1>Adicionar um novo produto</h1>

        <Form
          ref={this.formEl}
        >
          <input
            ref={ref => { this.uploadInput = ref }}
            type="file"
            onChange={e => this.uploadFile(e)}
            className="hide"
          />

          <div
            className="photo-preview"
            style={{ backgroundImage: `url(${this.state.imageURL || addPhoto })` }}
            onClick={() => this.uploadInput.click()}
            title="Inserir foto"
          />

          <Field
            label="Nome do produto"
            isRequired
            validators={[
              <Validator
                func={value => value.length > 5}
                invalid="Este campo deve conter mais de 5 caracteres."
                valid="Certo!"
              />,
            ]}
          >
            <FieldText
              style={{ width: "100%"}}
              shouldFitContainer={true}
              name="name"
              id="name"
              value={this.state.name}
              placeholder="Insira o nome do seu produto"
              onChange={e => this.setState({ name: e.target.value })}
            />
          </Field>

          <Field
            label="Preço"
            isRequired
            validators={[
              <Validator
                func={value => value >= 3}
                invalid="O preço do produto deve ser maior que R$ 3,00."
                valid="Certo!"
              />,
            ]}
          >
            <FieldText
              type="number"
              name="price"
              id="price"
              value={this.state.price}
              onChange={e => this.setState({ price: e.target.value })}
            />
          </Field>

          <Field
            label="Estoque"
            isRequired
            validators={[
              <Validator
                func={value => value >= 1}
                invalid="O estoque do produto deve ser maior que 1."
                valid="Certo!"
              />,
            ]}
          >
            <FieldText
              type="number"
              name="stock"
              id="stock"
              value={this.state.stock}
              onChange={e => this.setState({ stock: e.target.value })}
            />
          </Field>

          <div className="form-actions">
            <Button
              style={{ float: "right" }}
              appearance="primary"
              isLoading={this.state.submittet}
              onClick={() => this.onSubmitHandler()}
            >
              Adicionar
            </Button>
          </div>

        </Form>
      </Content>
    ]
  }
}
