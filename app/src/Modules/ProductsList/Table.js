import React from 'react'
import PropTypes from 'prop-types'

import noPhoto from 'Assets/Images/nofoto.png'

const Table = props => (
  <div className="wrap-table100">
    <table className="table">

      <thead className="row header">
        <tr>
          <th className="cell"></th>
          <th className="cell">
            Nome do produto
          </th>
          <th className="cell">
            Estoque
          </th>
          <th className="cell">
            Preço original
          </th>
        </tr>
      </thead>

      <tbody>
        {
          props.products.map((product, index) => (
            <tr className="row" key={`product-${index}`}>
              <td className="cell" data-title="Imagem">
                { product.imageUrl
                  ? <img src={ product.imageUrl } width="50px" alt={ product.name } />
                  : <img src={noPhoto} width="50px" alt={ product.name } /> }
              </td>
              <td className="cell" data-title="Nome do produto">
                { product.name }
              </td>
              <td className="cell" data-title="Estoque">
                { product.stock || "-" }
              </td>
              <td className="cell" data-title="Preço original">
                { new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(product.price) }
              </td>
            </tr>
          ))
        }
      </tbody>
    </table>
  </div>
)

Table.propTypes = {
  products: PropTypes.array.isRequired
}

export default Table
