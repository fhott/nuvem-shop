import React from 'react'
import Pagination from '@atlaskit/pagination'
import Spinner from '@atlaskit/spinner'
import Content from 'Components/Content'
import Table from './Table'

import './ProductsList.sass'

export default class ProductsList extends React.Component {
  constructor() {
    super()

    this.state = {
      products: [],
      totalPages: 0,
      page: 1,
      loaded: false,
      error: false
    }

    this.fetchProductsList()
  }

  breadcrumbs = [
    {
      text: "Meus produtos",
      link: "/products"
    }
  ]

  fetchProductsList(page = 1) {
    this.setState({ loaded: false })

    return fetch(`http://localhost:4000/products?page=${page - 1}`,
      { method: 'GET',
        mode: 'cors',
        cache: 'default' })
        .then(response => response.json())
        .then(response => this.setState({ products: response.products,
          totalPages: response.totalOfPages,
          loaded: true }))
  }

  render() {
    return (
      <Content
      breadcrumbs={this.breadcrumbs}
      >
        <h1>Meus produtos</h1>

        {
          this.state.loaded
          ? <Table products={this.state.products} />
          : <Spinner />
        }

        {
          this.state.totalPages > 1 && (
            <div className="pagination-wrapper">
              <Pagination
                value={this.state.page}
                total={this.state.totalPages}
                onChange={page => {
                  this.fetchProductsList(page)
                  this.setState({ page })
                }}
              />
            </div>
          )
        }
      </Content>
    )
  }
}
