import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import { Grid, GridColumn } from '@atlaskit/page'
import Button from '@atlaskit/button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExternalLinkAlt, faPlusSquare, faListUl, faChevronRight } from '@fortawesome/free-solid-svg-icons'

import Logo from '../Logo'
import './Sidebar.sass'

const Sidebar = props => (
  <div className={`sidebar ${props.open ? 'active' : ''}`}>
    <div className="sidebar__header">
      <Grid>
        <GridColumn medium={4}>
          <Logo />
        </GridColumn>
        <GridColumn medium={8}>
          <h3>Minha Loja</h3>
          <p>Martín Palombo</p>
        </GridColumn>
        <GridColumn medium={12}>
          <Button
            href="https://www.nuvemshop.com.br"
            target="_blank"
          >
            <FontAwesomeIcon icon={faExternalLinkAlt} />
            <span className="external-link-text">minha-loja.lojavirtualnuvem.com.br</span>
          </Button>
        </GridColumn>
      </Grid>
    </div>

    <nav>
      <ul>
        <li>
          <NavLink
            to="/products"
            activeClassName="active"
            onClick={() => props.openSidebar()}
          >
            <FontAwesomeIcon icon={faListUl} />
            Produtos
            <FontAwesomeIcon icon={faChevronRight} />
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/product/new"
            activeClassName="active"
            onClick={() => props.openSidebar()}
          >
            <FontAwesomeIcon icon={faPlusSquare} className="first-icon" />
            Adicionar um novo produto
            <FontAwesomeIcon icon={faChevronRight} />
          </NavLink>
        </li>
      </ul>
    </nav>
  </div>
)

Sidebar.propTypes = {
  openSidebar: PropTypes.func,
  open: PropTypes.boolean
}

export default Sidebar
