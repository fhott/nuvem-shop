import React from 'react'
import PropTypes from 'prop-types'
import { BreadcrumbsStateless, BreadcrumbsItem } from '@atlaskit/breadcrumbs'

import './Content.sass'

const Content = props => (
  <div className="content">
    <BreadcrumbsStateless>
      {
        props.breadcrumbs.map((item, index) =>
          <BreadcrumbsItem key={`breadcrumb-item-${index}`} href={item.link} text={item.text} />)
      }
    </BreadcrumbsStateless>

    { props.children }
  </div>
)

Content.propTypes = {
  breadcrumbs: PropTypes.array,
  children: PropTypes.array
}

export default Content
