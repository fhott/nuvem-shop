import React from 'react'
import logo from 'Assets/Images/logo.png'

import './Logo.sass'

const Logo = () => (
    <img src={logo} alt="Nuvem Shop" className="logo" />
)

export default Logo
