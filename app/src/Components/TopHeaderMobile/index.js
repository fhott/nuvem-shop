import React from 'react'
import PropTypes from 'prop-types'
import Button from '@atlaskit/button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons'
import Burger from 'Components/Burger'

import Logo from 'Components/Logo'
import './TopHeaderMobile.sass'

const TopHeaderMobile = props => (
  <div className="top-header-mobile">
    <Burger
      onClick={props.openSidebar}
      open={props.open}
    />
    <div className="title">
      <Logo />
      <h2>Minha Loja</h2>
    </div>
    <Button
      href="https://www.nuvemshop.com.br"
      target="_blank"
    >
      <FontAwesomeIcon icon={faExternalLinkAlt} />
    </Button>
  </div>
)

TopHeaderMobile.propTypes = {
  openSidebar: PropTypes.func,
  open: PropTypes.bool
}

export default TopHeaderMobile
