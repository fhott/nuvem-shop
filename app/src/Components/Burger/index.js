import React from 'react'
import PropTypes from 'prop-types'

import './Burger.sass'

const Burger = props => (
  <div
    className={`burger ${props.open ? 'open' : ''}`}
    onClick={() => props.onClick()}
  >
    <span></span>
    <span></span>
    <span></span>
  </div>
)

Burger.propTypes = {
  onClick: PropTypes.func.isRequired
}


export default Burger
