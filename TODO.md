# Tienda Nube

## To Do list

### App

- [x] Initialize project from `create-react-app`
- [x] Sidebar component
- [x] Content layout component
- [x] Product form
- [x] Products List component
- [x] Pagination component
- [x] Docker

### API

- [x] Initialize project
- [x] Initialize product basic list
- [x] Add new product route
- [x] List products route
- [x] Docker

### Fixes and tests

- [ ] Fix app build
- [ ] Fix Docker
- [ ] Unit tests
- [ ] Component tests
- [ ] E2E tests