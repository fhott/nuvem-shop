![Nuvem Shop](https://www.tiny.com.br/images/site/ecommerce/nuvemshoph.png?v=1536783782)

Technical challenge for the Nuvem Shop selection process

## Running the API

- Enter the `api` directory

- Install the dependencies running `npm install` or `yarn`

- Run the application with `npm run dev` or `yarn run dev`

- To run the built project, run `npm run build` or `yarn run build`, after that, run `npm start` or `yarn start`

## Running the App

- Enter the `app` directory

- Install the dependencies running `npm install` or `yarn`

- Run the application with `npm start` or `yarn start`