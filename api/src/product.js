export default class Products {
    constructor() {
        this.list = [
            { name: 'Product 1', price: 10.97, stock: 3 },
            { name: 'Product 2', price: 2.30, stock: 4 },
            { name: 'Product 3', price: 13.27 },
            { name: 'Product 4', price: 20.99, stock: 34 },
            { name: 'Product 5', price: 31.98, stock: 2 },
            { name: 'Product 6', price: 6.97 },
            { name: 'Product 7', price: 31.98 },
            { name: 'Product 8', price: 6.97 },
            { name: 'Product 9', price: 31.98, stock: 23 },
            { name: 'Product 10', price: 6.97, stock: 12 },
            { name: 'Product 11', price: 31.98 },
            { name: 'Product 12', price: 6.97 },
            { name: 'Product 13', price: 31.98 },
            { name: 'Product 14', price: 6.97 },
            { name: 'Product 15', price: 31.98, stock: 34 },
            { name: 'Product 16', price: 6.97 },
            { name: 'Product 17', price: 20.99 },
            { name: 'Product 18', price: 31.98, stock: 23 },
            { name: 'Product 19', price: 6.97 },
            { name: 'Product 20', price: 31.98 },
            { name: 'Product 21', price: 6.97 },
            { name: 'Product 22', price: 10.97, stock: 22 },
            { name: 'Product 23', price: 2.30, stock: 2 },
            { name: 'Product 24', price: 13.27, stock: 3 },
            { name: 'Product 25', price: 20.99, stock: 1 },
        ]
    }

    add(product) {
        this.list.push(product)
    }

    get() {
        // create new array
        return this.list.slice().reverse() 
    }
}
