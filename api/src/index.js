import http from 'http'
import path from 'path'
import express from 'express'
import morgan from 'morgan'
import uuid from 'uuid'
import cors from 'cors'
import bodyParser from 'body-parser'
import Products from './product'
import fileUpload from 'express-fileupload'

let app = express()
app.server = http.createServer(app)

// logger
app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(cors())
app.use(fileUpload())
app.use(express.static('public'));

const products = new Products()

app.get('/products', (req, res) => {
    const pageNumber = req.query.page || 0
    const pageSize = 10
    
    const response = {}

    response.products = products.get().slice(pageNumber * pageSize, (pageNumber * pageSize) + pageSize)
    response.totalOfPages = Math.ceil(products.list.length / pageSize)
    response.pageNumber = Number(pageNumber)
    response.pageSize = pageSize

    res.json(response)
})

app.post('/product', (req, res) => {
    const { product }  = req.body

    if (!product || !product.name || !product.price) {
        res.status(500).json({ error: 'Product or product attributes are missing' })
        return
    }

    products.add(product)

    res.status(201).json({ status: 'success', product })
})

app.post('/upload', (req, res) => {
    let imageFile = req.files.file
    const fileName = uuid()

    imageFile.mv(`./public/files/${fileName}.jpg`, function(err) {
        if (err) {
            return res.status(500).send(err)
        }

        res.json({file: `${fileName}.jpg`})
    });

})

app.get('/', (req, res) => {
    res.json({
        app: 'Nuvem Shop challenge',
        author: 'Fillipe Hott',
        version: '0.1.0'
    })
})

app.server.listen(4000, () => {
    /* eslint no-console: 0, */
    console.log(`Started on port ${app.server.address().port}`)
});

export default app